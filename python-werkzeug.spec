%global _empty_manifest_terminate_build 0
%global pypi_name werkzeug

Name:		python-werkzeug
Version:	3.1.3
Release:	1
Summary:	The comprehensive WSGI web application library.
License:	BSD-3-Clause
URL:            https://github.com/pallets/werkzeug
Source0:        %{url}/archive/%{version}/%{pypi_name}-%{version}.tar.gz

Patch0:         preserve-any-existing-PYTHONPATH-in-tests.patch

BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-hatchling
BuildRequires:  python3-flit-core
BuildRequires:  python3-markupsafe
BuildRequires:  python3-ephemeral-port-reserve

%description
*werkzeug* German noun: "tool". Etymology: *werk* ("work"), *zeug* ("stuff")
Werkzeug is a comprehensive `WSGI`_ web application library. It began as
a simple collection of various utilities for WSGI applications and has
become one of the most advanced WSGI utility libraries.
It includes:
-   An interactive debugger that allows inspecting stack traces and
    source code in the browser with an interactive interpreter for any
    frame in the stack.
-   A full-featured request object with objects to interact with
    headers, query args, form data, files, and cookies.
-   A response object that can wrap other WSGI applications and handle
    streaming data.
-   A routing system for matching URLs to endpoints and generating URLs
    for endpoints, with an extensible system for capturing variables
    from URLs.
-   HTTP utilities to handle entity tags, cache control, dates, user
    agents, cookies, files, and more.
-   A threaded WSGI server for use while developing applications
    locally.
-   A test client for simulating HTTP requests during testing without
    requiring running a server.
Werkzeug doesn't enforce any dependencies. It is up to the developer to
choose a template engine, database adapter, and even how to handle
requests. It can be used to build all sorts of end user applications
such as blogs, wikis, or bulletin boards.
`Flask`_ wraps Werkzeug, using it to handle the details of WSGI while
providing more structure and patterns for defining powerful
applications.

%package -n python3-werkzeug
Summary:	The comprehensive WSGI web application library.
Provides:	python-werkzeug
BuildRequires:  python3-pytest
BuildRequires:	python3-pytest-xprocess
BuildRequires:  python3-pytest-timeout
BuildRequires:  python3-requests
BuildRequires:  python3-cryptography
BuildRequires:  python3-greenlet
BuildRequires:  python3-watchdog
BuildRequires:  python3-time-machine
BuildRequires:  python3-pytest-mock
BuildRequires:  python3-pytest-cov
BuildRequires:  python3-pytest-rerunfailures
BuildRequires:  python3-pytest-flake8
BuildRequires:  python3-hypothesis
BuildRequires:  python3-pytest-xdist
BuildRequires:  python3-pytest-datadir
BuildRequires:  python3-pytest-asyncio
BuildRequires:  python3-anyio
BuildRequires:  python3-pytest-trio
%description -n python3-werkzeug
*werkzeug* German noun: "tool". Etymology: *werk* ("work"), *zeug* ("stuff")
Werkzeug is a comprehensive `WSGI`_ web application library. It began as
a simple collection of various utilities for WSGI applications and has
become one of the most advanced WSGI utility libraries.
It includes:
-   An interactive debugger that allows inspecting stack traces and
    source code in the browser with an interactive interpreter for any
    frame in the stack.
-   A full-featured request object with objects to interact with
    headers, query args, form data, files, and cookies.
-   A response object that can wrap other WSGI applications and handle
    streaming data.
-   A routing system for matching URLs to endpoints and generating URLs
    for endpoints, with an extensible system for capturing variables
    from URLs.
-   HTTP utilities to handle entity tags, cache control, dates, user
    agents, cookies, files, and more.
-   A threaded WSGI server for use while developing applications
    locally.
-   A test client for simulating HTTP requests during testing without
    requiring running a server.
Werkzeug doesn't enforce any dependencies. It is up to the developer to
choose a template engine, database adapter, and even how to handle
requests. It can be used to build all sorts of end user applications
such as blogs, wikis, or bulletin boards.
`Flask`_ wraps Werkzeug, using it to handle the details of WSGI while
providing more structure and patterns for defining powerful
applications.

%package help
Summary:	Development documents and examples for Werkzeug
Provides:	python3-werkzeug-doc
%description help
*werkzeug* German noun: "tool". Etymology: *werk* ("work"), *zeug* ("stuff")
Werkzeug is a comprehensive `WSGI`_ web application library. It began as
a simple collection of various utilities for WSGI applications and has
become one of the most advanced WSGI utility libraries.
It includes:
-   An interactive debugger that allows inspecting stack traces and
    source code in the browser with an interactive interpreter for any
    frame in the stack.
-   A full-featured request object with objects to interact with
    headers, query args, form data, files, and cookies.
-   A response object that can wrap other WSGI applications and handle
    streaming data.
-   A routing system for matching URLs to endpoints and generating URLs
    for endpoints, with an extensible system for capturing variables
    from URLs.
-   HTTP utilities to handle entity tags, cache control, dates, user
    agents, cookies, files, and more.
-   A threaded WSGI server for use while developing applications
    locally.
-   A test client for simulating HTTP requests during testing without
    requiring running a server.
Werkzeug doesn't enforce any dependencies. It is up to the developer to
choose a template engine, database adapter, and even how to handle
requests. It can be used to build all sorts of end user applications
such as blogs, wikis, or bulletin boards.
`Flask`_ wraps Werkzeug, using it to handle the details of WSGI while
providing more structure and patterns for defining powerful
applications.

%prep
%autosetup -n werkzeug-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/doclist.lst .

%check
PYTHONPATH=%{buildroot}%{python3_sitelib} pytest -k 'not (test_serving)'

%files -n python3-werkzeug
%license LICENSE.txt
%doc README.md CHANGES.rst
%{python3_sitelib}/werkzeug
%{python3_sitelib}/werkzeug-*.dist-info/

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Dec 05 2024 xu_ping <707078654@qq.com> - 3.1.3-1
- Update package to version 3.1.3
  * Drop support for Python 3.8.
  * Remove previously deprecated code.
  * Add 421 MisdirectedRequest HTTP exception.
  * Fix an issue that caused str(Request.headers) to always appear empty.

* Thu Oct 31 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 3.0.6-1
- Update package to version 3.0.6
  Fix how max_form_memory_size is applied when parsing large non-file fields.
  The Watchdog reloader ignores file closed no write events.
  Logging works with client addresses containing an IPv6 scope.
  Ignore invalid authorization parameters.
  Improve type annotation fore SharedDataMiddleware.
  Make reloader more robust when "" is in sys.path.

* Tue May 07 2024 yinyongkang <yinyongkang@kylinos.cn> - 2.2.3-2
- fix CVE-2024-34069

* Tue May 09 2023 wulei <wu_lei@hoperun.com> - 2.2.3-1
- Update to 2.2.3

* Sat Jan 7 2023 Bolehu <heyaohua@xfusion.com> - 2.0.3-2
- fix typo and grammar mistake

* Fri Jun 17 2022 jiangpengju <jiangpengju2@h-partners.com> - 2.0.3-1
- Upgrade python-werkzeug version to 2.0.3

* Mon Nov 15 2021 xu_ping <xuping33@huawei.com>-1.0.1-2
- fix test failures due to unhandled exceptions being thrown without being propagated to caller.

* Thu Feb 04 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
